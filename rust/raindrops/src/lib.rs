
pub fn raindrops(n: u32) -> String {
    if !has_any_factors(n, vec![3, 5, 7]) {
        n.to_string()
    } else {
        let mut output = String::new();
        
        for factor_string in factors_of(n).into_iter().map(factor_to_string) {
            output.push_str(factor_string);            
        }
        output
    }
}

fn has_any_factors(n: u32, factors: Vec<u32>) -> bool {
    factors.iter().
        fold(false, |check, factor| { check || n % factor == 0 })
}

#[test]
fn factor_is_self() { assert_eq!(has_any_factors(32, vec![32]), true) }

#[test]
fn factor_is_1() { assert_eq!(has_any_factors(32, vec![1]), true) }

#[test]
fn factor_is_1_and_self() { assert_eq!(has_any_factors(32, vec![32,1]), true) }

#[test]
fn single_not_a_factor() { assert_eq!(has_any_factors(32, vec![13]), false) }

#[test]
fn mixed() { assert_eq!(has_any_factors(32, vec![16, 13]), true) }

#[test]
fn many_factors() { assert_eq!(has_any_factors(32, vec![2, 16, 4, 8]), true) }

fn factors_of(n: u32) -> Vec<u32> {
    let mut unsorted_factors = (1..n/2 + 1). // want to catch low odd numbers basically
        filter(|x| { n % x == 0 }).
        flat_map(|x| { vec![x, n/x] }).
        collect::<Vec<u32>>();
    unsorted_factors.sort();
    unsorted_factors.dedup_by_key(|x| { *x });
    unsorted_factors
}

#[test]
fn three_has_two_factors() { assert_eq!(factors_of(3), vec![1, 3]) }

#[test]
fn prime_has_two_factors() { assert_eq!(factors_of(13), vec![1, 13]) }

#[test]
fn zero_has_no_factors() { assert_eq!(factors_of(0), vec![]) }

#[test]
fn simple_correct_factors() { assert_eq!(factors_of(12), vec![1, 2, 3, 4, 6, 12]) }

#[test]
fn simple_correct_factors2() { assert_eq!(factors_of(10), vec![1, 2, 5, 10]) }

fn factor_to_string(n: u32) -> &'static str {
    match n {
        3 => "Pling",
        5 => "Plang",
        7 => "Plong",
        _ => ""
    }
}
